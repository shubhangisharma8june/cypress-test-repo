/// <reference types ="cypress" />

import selectFlight from './SelectFlights.page';
import { And } from 'cypress-cucumber-preprocessor/steps';

And('Verify select flight page', ()=>{
    selectFlight.verifyPageTitle();
})

And('Apply duration filter', ()=>{
    selectFlight.durationFilter();
})

And('Apply price filter', ()=>{
    selectFlight.priceFilter();
})

And('Select continue after selecting fight', ()=>{
    selectFlight.continueButton();
})

And('Select close icon on upgrade popup', ()=>{
    selectFlight.closePopup();
})

And('Select Next on contact details modal', ()=>{
    selectFlight.contactDetailsmodal();
})