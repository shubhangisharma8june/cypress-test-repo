/// <reference types ="cypress" />
Cypress.on('uncaught:exception', () => false);

import bookFlight from './BookFlight.page';
import { And } from 'cypress-cucumber-preprocessor/steps';

// And('Verify book flight tab and one-way option on Book a flight block', ()=>{
//     bookFlight.bookAflightBlock();
// })

// And('Search and select origin', ()=>{
//     bookFlight.enterOrigin();
// })

And('Search and select Destination', ()=>{
    bookFlight.enterDestination();
})

And('Select travel date', ()=>{
    bookFlight.selectDepartureDate();
})

And('Select Search flights button on the block', ()=>{
    bookFlight.selectSearchFlightButton();
})