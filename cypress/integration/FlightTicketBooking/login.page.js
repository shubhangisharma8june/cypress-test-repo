/// <reference types ="cypress" />

class homePage{
    static landingPage(){
        cy.visit('https://www.goindigo.in/');
    }

    static loginButton(){
        cy.get('[title=Login]').click({force: true}).contains('Login');

    }

    static enterPhoneNumber(){
        cy.get('#memberId').type("7011243294");
    }

    static enterPassword(){
        cy.get('[type=password]').type("Test123");
    }

    static selectLoginButton(){
        cy.get('.buttonText').click();

        cy.on('window:alert', (str)=>{
            expect(str).to.equal('You have been logged in successfully')
            cy.wait(10000);

        })
    }
}

export default homePage;