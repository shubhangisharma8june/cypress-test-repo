/// <reference types ="cypress" />
Cypress.on('uncaught:exception', () => false);

import { And } from 'cypress-cucumber-preprocessor/steps';
import homePage from './login.page';

And('Visit website', ()=>{
    homePage.landingPage();
})

And('Select Login button on the homepage', ()=>{
    homePage.loginButton();
})

And('Enter valid phone number', ()=>{
    homePage.enterPhoneNumber();
})

And('Enter valid password', ()=>{
    homePage.enterPassword();
})

And('Select login button on the modal', ()=>{
    homePage.selectLoginButton();
})