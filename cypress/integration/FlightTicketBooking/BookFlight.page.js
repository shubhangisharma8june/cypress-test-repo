/// <reference types ="cypress" />

class bookFlight {
    // static bookAflightBlock(){
    //     cy.get('#bookFlightTab').should('be.visible');
    // }

    // static enterOrigin(){
    //     cy.get('[name=or-src]').click().type('Del').type('{enter}');
    // }

    static enterDestination(){
        cy.wait(10000);
        cy.get('[name=or-dest]').click({ force: true }).type('BOM').type('{enter}');
    }

    static selectDepartureDate(){
        cy.get('div.row.justify-content-around.ie-justify-dest > div.col-sm-5.col-5.padd-left > div > input').click();
        cy.get('[data-month="7"]').eq(7).click()
    }

    static selectSearchFlightButton(){
        cy.get('.hp-src-btn').click({ force: true });
        cy.wait(10000);
        
    }

}

export default bookFlight;