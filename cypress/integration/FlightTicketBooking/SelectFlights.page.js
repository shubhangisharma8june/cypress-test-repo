/// <reference types ="cypress" />

class selectFlight{
    static verifyPageTitle(){
        cy.title().should('eq', 'Select Flights for Domestic & International Destinations | IndiGo');
    }

    static durationFilter(){
        cy.get('div.filters-trip.d-none.d-md-block > div > div > div > div:nth-child(5)').dblclick();
    }

    static priceFilter(){
        cy.get('div.filters-trip.d-none.d-md-block > div > div > div > div:nth-child(4)').dblclick();
    }

    static continueButton(){
        cy.get('#continue-button').contains("Continue")
        .click({force:true});
    }

    static closePopup(){
        cy.get('.closeBtn').click();
    }

    static contactDetailsmodal(){
        cy.get('[type=submit]').click();
    }

}

export default selectFlight;