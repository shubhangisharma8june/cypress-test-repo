Feature: FlightTicketBooking

    Scenario: Given login 
    * Visit website
    * Select Login button on the homepage
    * Enter valid phone number
    * Enter valid password
    * Select login button on the modal


    Scenario: Book A Flight | Enter details
    # * Verify book flight tab and one-way option on Book a flight block
    * Search and select Destination
    * Select travel date
    * Select Search flights button on the block

    Scenario: Select a flight from the list
    * Verify select flight page
    * Apply duration filter
    * Apply price filter
    * Select continue after selecting fight
    * Select close icon on upgrade popup
    * Select Next on contact details modal
    